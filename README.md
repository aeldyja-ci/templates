# CI/CD Templates

A collection of ci/cd templates files to automate projects pipelines

## Getting Started

Include the file you want in any other project by following this exemple

```yaml
include:
  - project: 'path/to/this/project'
    file: '.template-file-you-want-to-use.yml'
```
