image: node:lts

cache: &global_cache
  key: "${CI_COMMIT_REF_SLUG}"
  paths:
    - node_modules/
    - package-lock.json

stages:
  - setup
  - test
  - build
  - deploy

default:
  tags:
    - docker

setup:
  stage: setup
  rules:
    - when: always
  script:
    - echo "Setting up project, installing dependencies"
    - npm ci
    - echo "Setup finished"

test:
  stage: test
  rules:
    - when: always
  cache:
    # inherit all global cache settings
    <<: *global_cache
    # override the policy
    policy: pull
  script:
    - echo "Testing the project"
    - npm run test


.build:
  stage: build
  rules:
    - when: never
  cache:
    # inherit all global cache settings
    <<: *global_cache
    # override the policy
    policy: pull
  artifacts:
    paths:
      - build/
    expire_in: 1 week
  variables:
    REACT_APP_API_URL: "${CONFIG_API_URL}"
  script:
    - echo "Project is being built !"
    - npm run build
    - echo "Build successfully!"

build:dev:
  extends: .build
  rules:
    - if: '$CI_COMMIT_TAG == null && $CI_COMMIT_REF_NAME != "master"'
  variables:
    REACT_APP_ENVIRONMENT: "development"
  script:
    - unset CI
    - !reference [.build, script]

.deploy:
  stage: deploy
  rules:
    - when: never
  image: debian:latest
  cache: {}
  before_script:
    - echo "Before script section"
    - echo "Installing packages"
    - 'which ssh-agent || ( apt-get update -y && apt-get install openssh-client rsync -y )'
    - eval $(ssh-agent -s)
    - echo "${SSH_DEPLOY_KEY:? Missing SSH_DEPLOY_KEY}" | tr -d '\r' | ssh-add -
    - mkdir -p ~/.ssh
    - chmod 700 ~/.ssh
    - echo "${SSH_KNOWN_HOSTS_FILE_SERVER:?Missing SSH_KNOWN_HOSTS_FILE_SERVER}" >> ~/.ssh/known_hosts
    - chmod 644 ~/.ssh/known_hosts
  script:
    - echo "No deploy script defined in pipeline"

deploy:dev:
  extends: .deploy
  rules:
    - if: '$CI_COMMIT_TAG == null && $CI_COMMIT_REF_NAME != "master"'
  script:
    - rsync -avP -e "ssh -p ${SSH_DEPLOY_SERVER_PORT:?Missing SSH_DEPLOY_SERVER_PORT}" build/ "gitlab-deploy@${SSH_DEPLOY_SERVER_IP:?Missing SSH_DEPLOY_SERVER_IP}:${SSH_DEPLOY_PATH_DYNAMIC:?Missing SSH_DEPLOY_PATH_DYNAMIC}/${CI_PROJECT_NAME}-${CI_COMMIT_REF_SLUG}" --delete
  needs:
    - job: build:dev
      artifacts: true
  environment:
    name: dev/$CI_COMMIT_REF_NAME
    deployment_tier: development
    url: https://${CI_PROJECT_NAME}-${CI_COMMIT_REF_SLUG}.${ENV_BASE_URL}/
    on_stop: stop_deploy:dev
    auto_stop_in: 1 week

stop_deploy:dev:
  extends: .deploy
  rules:
    - if: '$CI_COMMIT_TAG == null && $CI_COMMIT_REF_NAME != "master"'
      when: manual
  variables:
    GIT_STRATEGY: none
  script:
    - echo "Cleaning deploy environment"
    - ssh  -p "${SSH_DEPLOY_SERVER_PORT:?Missing SSH_DEPLOY_SERVER_PORT}" "gitlab-deploy@${SSH_DEPLOY_SERVER_IP:?Missing SSH_DEPLOY_SERVER_IP}" "rm -rv ${SSH_DEPLOY_PATH_DYNAMIC:?Missing SSH_DEPLOY_PATH_DYNAMIC}/${CI_PROJECT_NAME}-${CI_COMMIT_REF_SLUG}"
  needs:
    - job: deploy:dev
  environment:
    name: dev/$CI_COMMIT_REF_NAME
    action: stop
